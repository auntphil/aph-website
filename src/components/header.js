import PropTypes from 'prop-types'
import React from 'react'

const logo = require('../images/logo-small.png')

const Header = ({ siteTitle }) => (
  <div id="header">
    <div className="wrapper">
      <img src={logo} alt="APH Logo" id="logo" />
    </div>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
