import React from 'react'

const monthAbr = [`Jan`,`Feb`,`Mar`,`Apr`,`May`,`Jun`,`Jul`,`Aug`,`Sep`,`Oct`,`Nov`,`Dec`]

function formatPublishedDate( d ){
    let publishedAt = new Date(d)
    return `${publishedAt.getFullYear()}, ${monthAbr[publishedAt.getMonth()]}`
}

const Project = (props) =>
    <>
    {
        props.projects.map( project => (
            <div className="project" key={project._id}>
                <div className="projectTitle">
                    <h2>{project.title} <span className="publishedAt">{`${formatPublishedDate(project.publishedAt)}`}</span></h2>
                </div>
                <div className="projectDesc">
                    {project.body.map( paragraph => (
                        <p key={paragraph._key} >
                            {paragraph.children.map( child => (
                                <React.Fragment key={child._key} >
                                    {child.text}
                                </React.Fragment>
                            ))}
                        </p>
                    ))}
                    {project.disclaimer !== undefined &&
                        <div className="disclaimer">
                            {project.disclaimer.map( paragraph => (
                                <p key={paragraph._key} >
                                    {paragraph.children.map( child => (
                                        <React.Fragment key={child._key} >
                                            {child.text}
                                        </React.Fragment>
                                    ))}
                                </p>
                            ))}
                        </div>
                    }
                </div>
                <ul className="projectFooter">
                    {project.links.map( link => (
                        <li key={link._id}>
                            <a href={link.url} target="_blank" rel="noreferrer noopener">{link.title}</a>
                        </li>
                    ))}
                </ul>
            </div>
        ))
    }
    </>

export default Project