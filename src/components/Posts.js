import React, { Component } from 'react'
import Parser from 'rss-parser'

const monthAbr = [`Jan`,`Feb`,`Mar`,`Apr`,`May`,`Jun`,`Jul`,`Aug`,`Sep`,`Oct`,`Nov`,`Dec`]

function formatPubDate( pubDate ){
    let publishedAt = new Date(pubDate)
    return `${publishedAt.getDate()} ${monthAbr[publishedAt.getMonth()]}, ${publishedAt.getFullYear()}`
}

class Posts extends Component {
    state = {
        loading: true,
        posts: null,
        error: false,
        errorMsg: null,
    }

    componentDidMount() {
        let parser = new Parser()
        parser.parseURL('https://techsetta.com/feed/', (err, feed) => {
            if(err){
                this.setState({
                    loading: false,
                    error: true,
                    errorMsg: `Could not load RSS feed.`
                })
            }else{
                for(let x = 0; x < feed.items.length; x++){
                    const snip = feed.items[x].contentSnippet.split(" ")
                    feed.items[x].contentShort = ''
                    for(let y = 0; y < 20; y++){
                        feed.items[x].contentShort += `${snip[y]} `
                    }
                }
                this.setState({
                    loading:false,
                    posts: feed,
                })
            }
        })
    }

    render(){
        return(
            <div id="blog-posts">
                { this.state.loading &&
                    `Loading new posts....please hold.`
                }
                { this.state.error &&
                    `Sadly posts cannot be loaded at this time. :(`
                }
                { this.state.posts !== null &&
                    this.state.posts.items.map( post => {
                        return(
                            <div key={post.guid} className="post">
                                <span className="publishedAt">{formatPubDate(post.pubDate)}</span>
                                <h3 className="postTitle">{post.title}</h3>
                                <p className="postContent">{post.contentShort.trim()}...</p>
                                <a href={`${post.link}?utm_source=aph&utm_medium=read-more`} target="_blank">Read More</a>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default Posts