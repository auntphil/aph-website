import React from 'react'

const About = () => (
    <div id="about">
        <div className="textblock">
            <p>
                For more details on my career, please visit my <a href="https://www.linkedin.com/in/andrewhochmuth/" target="_blank" rel="noreferrer noopener">LinkedIn Profile</a>.
            </p>
        </div>
    </div>
)

export default About