import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import Header from './header'
import './layout.css'
import './style.sass'
import './slideshow.sass'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <div id="pageWrapper">
        <Header siteTitle={data.site.siteMetadata.title} />
        <div id="content">
          {children}
        </div>
        <footer id="footer">
          © {new Date().getFullYear()} APH |&nbsp;<a href="https://gitlab.com/auntphil" target="_blank" rel="noreferrer noopener">GitLab</a>&nbsp;|&nbsp;<a href="https://techsetta.com/?utm_source=aph&utm_medium=footer" target="_blank" rel="noreferrer noopener">Techsetta</a>
        </footer>
      </div>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
