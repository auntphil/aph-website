import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Project from '../components/Project';
import About from '../components/About';
import Posts from '../components/Posts';

let slideIndex = 0

function changeSlide(){
  let i
  const slides = document.getElementsByClassName("slide")
  for(i=0;i<slides.length;i++){
    slides[i].classList.remove('fadeIn')
    slides[i].classList.add('fadeOut')
  }
  
  slideIndex++
  if(slideIndex > slides.length){slideIndex = 1} 
  slides[slideIndex-1].classList.remove('fadeOut')
  slides[slideIndex-1].classList.add('fadeIn')
  setTimeout(changeSlide, 3000)
}

/**
 * Sanity Initialize
 */
const sanityClient = require('@sanity/client')
const client = sanityClient({
  projectId: 'c4jrlxee',
  dataset: 'production',
  token: '',
  useCdn: true,
})

class IndexPage extends React.Component {
  state = {
    loadingProjects: true,
    projects: [],
    heroDescriptors: ['Technology Consultant'],
  }

  componentDidMount(){

    const descriptorsQuery = ` *[_type == 'descriptor' && name == 'Hero'] { _id, name, list[] }`
    client.fetch(descriptorsQuery).then( descriptors => {
      this.setState({
        heroDescriptors: descriptors[0].list
      })
      changeSlide()
    })


    const projectsQuery = ` *[_type == 'project'] | order(publishedAt desc) { _id, title, body, disclaimer, links[]->{_id, title, url}, publishedAt }`
    client.fetch(projectsQuery).then( ( projects ) => {
      this.setState({
        loadingProjects: false,
        projects
      })
    })
  }

  render(){
    return(
      <Layout>
        <SEO title="Home" keywords={[`aph`, `technology`, `IT`, `application`, `react`]} />
        <div id="hero">
          <div id="heroContentWrapper">
            <div className="heroContent heroLeft"></div>
            <div className="heroContent heroRight">
              <h1>Andrew Hochmuth</h1>
              <div id="aboutListWrapper">
                Over <span className="bold">{ new Date().getFullYear() - 2010 } Years</span> Experience in the IT Field
                <ul id="slideshow">
                  { this.state.heroDescriptors.map( (d, index ) => (
                    <li className="slide" key={index} >{d}</li>
                  ))}
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div id="blog">
          <div className="innerContent">
            <a href="https://techsetta.com/?utm_source=aph&utm_medium=logo" target="_blank"><img src="https://techsetta.com/wp-content/uploads/2019/03/Techsetta-LogoName.svg" alt="Techsetta Logo" id="techsettaLogo" /></a>
            <p className="blog-desc">Techsetta is a different kind of tech website. Other websites are filled with knowledge and in-depth analysis of technical topics, however, differences in terminology and lingo; make them hard to read and understand. This is the very problem Techsetta fixes by writing with the average user in mind. We bring a new type of knowledge to the people.</p>
            <Posts />
          </div>
        </div>
        <div id="Projects">
          <div className="innerContent">
            <div id="projects">
              <h1 className="title">Projects</h1>
              <div id="projectWrapper">
                {(this.state.loadingProjects
                  ? <h1>Loading</h1>
                  : <Project projects={this.state.projects} />
                  )}
              </div>
                <p className="disclaimer textblock" style={{paddingTop: `0px`}}>My projects rely heavily on JavaScript/React and the use of a modern browser. I would suggest <a href="https://www.mozilla.org/en-US/firefox/new/" rel="noopener noreferrer" target="_blank">Chrome</a> or <a href="https://www.google.com/chrome/" rel="noopener noreferrer" target="_blank">Firefox</a> if you need a new one.</p>
              </div>
            <About />
          </div>
        </div>
        
      </Layout>
    )
  }
}

export default IndexPage
